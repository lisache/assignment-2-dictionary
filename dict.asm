%define POINTER_SIZE 8
extern string_equals
global find_word
section .text


;rdi - Указатель на нуль-терминированную строку
;rsi - Указатель на начало словаря
find_word:
	xor rax, rax
	.loop:
		test rsi, rsi          ; проверка на конец словаря
		je .end
		
		push rdi               ; сохранить caller-saved регистры
		push rsi
		add rsi, POINTER_SIZE             ; увеличиваем указатель на 8, чтобы получить указатель на ключ
		call string_equals     ; вызов функции сравнения строк
		pop rsi
		pop rdi

		cmp rax, 1             ; по результату выполнения функции string_equals в rax запишется либо 0 либо 1
		je .found
		
		mov rsi,[rsi]	       ; в rsi - указатель на след элемент

		
		jmp .loop
	.found:
	
		mov rax, rsi
		ret
	
	.end:
		xor rax, rax
		ret
%include "colon.inc"					
%include "words.inc"
%include "lib.h"
%define POINTER_SIZE 8
%define SYS_WRITE 1
%define FD_STDERR 1

section .bss

BUFFER: resb 256

section .rodata
not_found_msg: db "Ключ не найден", 0
limit_exceeded_msg: db "Ошибка ввода", 0
BUF_SIZE: db 255

global _start
section .text
					
print_error:     ; аналогичная функции print_string, но с выводом сообщения об ошибке в stderr
    push rdi     ; сохранить caller-saved регистры
    call string_length 
    pop rdi
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, SYS_WRITE 
    mov rdi, FD_STDERR   ; дескриптор stderr
    syscall 
    call print_newline_err		
    ret

_start:							    
    mov rdi, BUFFER	  ; данные для функции read_word: в rdi адрес начала буфера, в rsi - размер буфера 				
    mov rsi, BUF_SIZE
    push rdi						
    call read_word
    pop rdi						
    test rax, rax        ; если после выполнения read_word в rax оказался 0, значит слово слишком большое для буфера
    je .error		 ; переход к выводу сообщения об ошибке			
    mov rdi, rax	 ; в rdi - адрес буфера				
    mov rsi, pointer     ; в rsi указатель на начало словаря
    call find_word	 					
    test rax, rax	 ; если ключ не найден, переход к выводу сообщения 					
    je .key_not_found				
    add rax, POINTER_SIZE		 ; если ключ найден, функция возвращает адрес начала вхождения в словарь				
    mov rdi, rax						
    push rax							
    call string_length					
    pop rdi		  					   
    add rdi, rax         ; адрес начала ключа + длина ключа					
    inc rdi							    
    call print_string	 ; вывод значения			
    jmp .end							

.error:							
    mov rdi, limit_exceeded_msg					
    call print_error
    jmp .end							

.key_not_found:						
    mov rdi, not_found_msg			
    call print_error
    jmp .end				

.end:							       	
    jmp exit



















